package model;

import java.util.Map;

public interface CarDatabase {
    void createCarDatabase();

   Map<Integer, Car> getMapOfCars();

    Map<Integer, Car> getCarsUnderBudgetOf(int budget);
}
