package model;

public class Client {
    private String name;
    private int budget;

    public Client(int budget) {
        this.budget = budget;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }


}
