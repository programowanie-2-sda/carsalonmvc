package model.components;

public enum Interior {
    VELOUR(0),
    LEATHER(50),
    LEATHER_QUILTED(80);

    private int price;

    Interior(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

}
