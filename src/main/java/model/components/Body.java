package model.components;

public enum Body {
    SEDAN(0),
    HATCHBACK(200),
    KOMBI(300),
    PICKUP(400);

    private int price;

    Body(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
