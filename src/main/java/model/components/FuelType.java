package model.components;

public enum FuelType {
    PETROL(0),
    DIESEL(60),
    HYBRID(75);

    private int price;

    FuelType(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}

