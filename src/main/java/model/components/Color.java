package model.components;

public enum Color {
    RED(0),
    BLUE(20),
    BLACK(30);

    private int price;

    Color(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
