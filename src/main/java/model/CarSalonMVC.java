package model;

public interface CarSalonMVC {

    interface Controller{
        void attach(View view);
        void detach();
        void printWelcome();
        void start();
        void chooseBody();
        void chooseColor();
        void chooseInterior();
        void chooseFuelType();
        void finish();
    }

    interface View{
        void showConfiguredCar(Car car);
    }
}
