package model;

import model.components.Body;
import model.components.Color;
import model.components.FuelType;
import model.components.Interior;

public class Car {
    private Body body;
    private Color color;
    private Interior interior;
    private FuelType fuelType;
    private String name;
    private int basePrice;

    public Car(String name, int basePrice) {
        this.name = name;
        this.basePrice = basePrice;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public int getBasePrice() {
        return basePrice;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Interior getInterior() {
        return interior;
    }

    public void setInterior(Interior interior) {
        this.interior = interior;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public int calculateCurrentPrice() {
        int bodyPrice;
        int colorPrice;
        int interiorPrice;
        int fuelTypePrice;

        if (body == null) {
            bodyPrice = 0;
        } else {
            bodyPrice = body.getPrice();
        }
        if (color == null) {
            colorPrice = 0;
        } else {
            colorPrice = color.getPrice();
        }
        if (interior == null) {
            interiorPrice = 0;
        } else {
            interiorPrice = interior.getPrice();
        }
        if (fuelType == null) {
            fuelTypePrice = 0;
        } else {
            fuelTypePrice = fuelType.getPrice();
        }
        return basePrice + bodyPrice + colorPrice + interiorPrice + fuelTypePrice;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name="+name+
                ", body=" + body +
                ", color=" + color +
                ", interior=" + interior +
                ", fuelType=" + fuelType +
                ", current price=" + calculateCurrentPrice() +
                '}';
    }


}

