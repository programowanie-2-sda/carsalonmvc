package model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CarDatabaseImpl implements CarDatabase {
    private Map<Integer, Car> mapOfCars = new HashMap<>();

    @Override
    public void createCarDatabase() {
        Car car1 = new Car("car1", 1000);
        Car car2 = new Car("car2", 1500);
        Car car3 = new Car("car3", 2000);
        Car car4 = new Car("car4", 2500);
        Car car5 = new Car("car5", 3000);
        Car car6 = new Car("car6", 3500);
        List<Car> carList = (List.of(car1, car2, car3, car4, car5, car6));
        for (int i = 0; i < carList.size(); i++) {
            mapOfCars.put(i + 1, carList.get(i));
        }
    }

    @Override
    public Map<Integer, Car> getMapOfCars() {
        return mapOfCars;
    }

    @Override
    public Map<Integer, Car> getCarsUnderBudgetOf(int budget) {
        return mapOfCars.entrySet().stream()
                .filter(entry -> entry.getValue().getBasePrice() <= budget)
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
    }
}
