package view;

import controller.*;
import controller.componentsselector.*;
import controller.userinput.UserBudgetGetter;
import controller.userinput.UserBudgetGetterImpl;
import controller.userinput.UserOptionGetter;
import controller.userinput.UserOptionGetterImpl;
import model.Car;
import model.CarSalonMVC;

public class MainApp implements CarSalonMVC.View {
    public static void main(String[] args) {
        PrinterWrapper printerWrapper = new PrinterWrapperImpl();
        ScannerWrapper scannerWrapper = new ScannerWrapperImpl();
        UserOptionGetter userOptionGetter = new UserOptionGetterImpl(scannerWrapper, printerWrapper);
        UserBudgetGetter userBudgetGetter = new UserBudgetGetterImpl(scannerWrapper, printerWrapper);
        BodySelector bodySelector = new BodySelectorImpl(userOptionGetter);
        ColorSelector colorSelector = new ColorSelectorImpl(userOptionGetter);
        InteriorSelector interiorSelector = new InteriorSelectorImpl(userOptionGetter);
        FuelTypeSelector fuelTypeSelector = new FuelTypeSelectorImpl(userOptionGetter);
        BasicCarSelector basicCarSelector = new BasicCarSelectorImpl(userOptionGetter);
        CarSalonImpl carSalon = new CarSalonImpl(printerWrapper, bodySelector, colorSelector, interiorSelector,
                basicCarSelector, fuelTypeSelector, userBudgetGetter);
        carSalon.attach(new MainApp());
        carSalon.printWelcome();
        carSalon.start();
        carSalon.chooseBody();
        carSalon.chooseColor();
        carSalon.chooseInterior();
        carSalon.chooseFuelType();
        carSalon.finish();
    }

    @Override
    public void showConfiguredCar(Car car) {
        System.out.println("Configured car: " + car);
    }
}
