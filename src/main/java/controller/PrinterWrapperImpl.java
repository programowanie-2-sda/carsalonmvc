package controller;

import model.Car;

import java.util.Map;

public class PrinterWrapperImpl implements PrinterWrapper {
    @Override
    public void printWelcome() {
        System.out.println("Welcome in our car salon");
    }

    @Override
    public void printMessage(String text) {
        System.out.println(text);
    }

    @Override
    public void printMapOfCars(Map<Integer, Car> mapOfCars) {
        System.out.println("Cars within your budget: ");
        mapOfCars.entrySet().stream()
                .forEach(entry-> System.out.println(entry.getKey() +" : "+ entry.getValue()));
    }


}
