package controller.userinput;

import java.util.List;

public interface UserOptionGetter {
    int getUserInput(List<Integer> list, String text);
}
