package controller.userinput;

import controller.PrinterWrapper;
import controller.ScannerWrapper;

import java.util.List;

public class UserOptionGetterImpl implements UserOptionGetter {
    ScannerWrapper scannerWrapper;
    PrinterWrapper printerWrapper;

    public UserOptionGetterImpl(ScannerWrapper scannerWrapper, PrinterWrapper printerWrapper) {
        this.scannerWrapper = scannerWrapper;
        this.printerWrapper = printerWrapper;
    }

    @Override
    public int getUserInput(List<Integer> list, String text) {
        int option = -1;
        boolean flag = true;
        while (flag) {
            printerWrapper.printMessage(text);
            option = scannerWrapper.nextInt();
            if (list.contains(option)) {
                flag = false;
            } else{
                printerWrapper.printMessage("Incorrect input, please try again.");
            }
        }
        return option;
    }
}
