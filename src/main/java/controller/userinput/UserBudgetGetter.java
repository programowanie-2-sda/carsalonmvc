package controller.userinput;

public interface UserBudgetGetter {
    int getBudget();
}
