package controller.userinput;

import controller.PrinterWrapper;
import controller.ScannerWrapper;

public class UserBudgetGetterImpl implements UserBudgetGetter {
    ScannerWrapper scannerWrapper;
    PrinterWrapper printerWrapper;

    public UserBudgetGetterImpl(ScannerWrapper scannerWrapper, PrinterWrapper printerWrapper) {
        this.scannerWrapper = scannerWrapper;
        this.printerWrapper = printerWrapper;
    }

    @Override
    public int getBudget() {
        boolean flag = true;
        int budget = 0;
        while (flag) {
            printerWrapper.printMessage("Please provide budget");
            budget = scannerWrapper.nextInt();
            flag = false;
            if (budget <= 0) {
                printerWrapper.printMessage("Budget has to be positive amount!");
                flag = true;
            }
        }
        return budget;
    }
}
