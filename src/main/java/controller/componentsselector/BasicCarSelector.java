package controller.componentsselector;

import model.Car;

import java.util.Map;

public interface BasicCarSelector {
    Car choose(Map<Integer, Car> carMap);
}
