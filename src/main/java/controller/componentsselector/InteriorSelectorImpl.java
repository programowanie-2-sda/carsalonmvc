package controller.componentsselector;

import controller.userinput.UserOptionGetter;
import model.components.Color;
import model.components.Interior;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class InteriorSelectorImpl implements InteriorSelector {
    UserOptionGetter userOptionGetter;

    public InteriorSelectorImpl(UserOptionGetter userOptionGetter) {
        this.userOptionGetter = userOptionGetter;
    }

    @Override
    public Interior choose() {
        List<Integer> list = createListOfOptions();
        int option = userOptionGetter.getUserInput(list, "Select Interior: press 1-VELOUR, 2-LEATHER, 3-LEATHER_QUILTED");
        return Interior.values()[option-1];
    }

    private List<Integer> createListOfOptions() {
        return Arrays.stream(Interior.values())
                .map(element -> element.ordinal())
                .map(integer -> integer+1)
                .collect(Collectors.toList());
    }
}
