package controller.componentsselector;

import controller.userinput.UserOptionGetter;
import model.Car;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BasicCarSelectorImpl implements BasicCarSelector {
    private UserOptionGetter userOptionGetter;

    public BasicCarSelectorImpl(UserOptionGetter userOptionGetter) {
        this.userOptionGetter = userOptionGetter;
    }

    @Override
    public Car choose(Map<Integer, Car> carMap) {
        List<Integer> list = createListOfOptions(carMap);
        int option = userOptionGetter.getUserInput(list, "Select car from catalogue");
        return carMap.get(option);
    }

    private List<Integer> createListOfOptions(Map<Integer, Car> carMap) {
        return carMap.entrySet().stream()
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());
    }
}
