package controller.componentsselector;

import controller.userinput.UserOptionGetter;
import model.components.Color;
import model.components.FuelType;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FuelTypeSelectorImpl implements FuelTypeSelector {
    UserOptionGetter userOptionGetter;

    public FuelTypeSelectorImpl(UserOptionGetter userOptionGetter) {
        this.userOptionGetter = userOptionGetter;
    }

    @Override
    public FuelType choose() {
        List<Integer> list = createListOfOptions();
        int option = userOptionGetter.getUserInput(list, "Select Fuel Type: press 1-PETROL, 2-DIESEL, 3-HYBRID");
        return FuelType.values()[option-1];
    }

    private List<Integer> createListOfOptions() {
        return Arrays.stream(FuelType.values())
                .map(element -> element.ordinal())
                .map(integer -> integer+1)
                .collect(Collectors.toList());
    }
}
