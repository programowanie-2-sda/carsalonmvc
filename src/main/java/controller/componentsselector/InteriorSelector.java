package controller.componentsselector;

import model.components.Interior;

public interface InteriorSelector {
    Interior choose();
}
