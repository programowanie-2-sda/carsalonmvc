package controller.componentsselector;

import model.components.Body;

public interface BodySelector {
    Body choose();
}
