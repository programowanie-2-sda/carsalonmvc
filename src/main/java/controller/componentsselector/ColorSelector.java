package controller.componentsselector;

import model.components.Color;

public interface ColorSelector {
    Color choose();
}
