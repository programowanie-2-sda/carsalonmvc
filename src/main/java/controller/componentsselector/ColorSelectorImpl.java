package controller.componentsselector;

import controller.userinput.UserOptionGetter;
import model.components.Color;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ColorSelectorImpl implements ColorSelector {
    UserOptionGetter userOptionGetter;

    public ColorSelectorImpl(UserOptionGetter userOptionGetter) {
        this.userOptionGetter = userOptionGetter;
    }

    @Override
    public Color choose() {
        List<Integer> list = createListOfOptions();
        int option = userOptionGetter.getUserInput(list, "Select Color: press 1-RED, 2-BLUE, 3-BLACK");
        return Color.values()[option-1];
    }

    private List<Integer> createListOfOptions() {
        return Arrays.stream(Color.values())
                .map(element -> element.ordinal())
                .map(integer -> integer+1)
                .collect(Collectors.toList());
    }
}
