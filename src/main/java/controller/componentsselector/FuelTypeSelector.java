package controller.componentsselector;

import model.components.FuelType;

public interface FuelTypeSelector {
    FuelType choose();
}
