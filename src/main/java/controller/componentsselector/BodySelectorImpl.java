package controller.componentsselector;

import controller.userinput.UserOptionGetter;
import model.components.Body;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BodySelectorImpl implements BodySelector {
    UserOptionGetter userOptionGetter;

    public BodySelectorImpl(UserOptionGetter userOptionGetter) {
        this.userOptionGetter = userOptionGetter;
    }

    @Override
    public Body choose() {
        List<Integer> list = createListOfOptions();
        int option = userOptionGetter.getUserInput(list, "Select Body: press 1-SEDAN, 2-HATCHBACK, 3-KOMBI, 4-PICKUP");
        return Body.values()[option-1];
    }

    private List<Integer> createListOfOptions() {
        return Arrays.stream(Body.values())
                .map(element -> element.ordinal())
                .map(integer -> integer+1)
                .collect(Collectors.toList());
    }
}
