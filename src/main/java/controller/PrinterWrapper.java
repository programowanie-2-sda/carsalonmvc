package controller;

import model.Car;

import java.util.Map;

public interface PrinterWrapper {
    void printWelcome();
    void printMessage(String text);
    void printMapOfCars(Map<Integer, Car> mapOfCars);
}
