package controller;

import controller.componentsselector.*;
import controller.userinput.UserBudgetGetter;
import model.*;
import model.components.Body;
import model.components.Color;
import model.components.FuelType;
import model.components.Interior;

import java.util.Map;

public class CarSalonImpl implements CarSalonMVC.Controller {

    private CarSalonMVC.View view;
    private PrinterWrapper printerWrapper;
    private BodySelector bodySelector;
    private ColorSelector colorSelector;
    private InteriorSelector interiorSelector;
    private BasicCarSelector basicCarSelector;
    private FuelTypeSelector fuelTypeSelector;
    private UserBudgetGetter userBudgetGetter;
    private CarDatabase carDatabase;
    private Car car;
    private Client client;

    public CarSalonImpl(PrinterWrapper printerWrapper, BodySelector bodySelector, ColorSelector colorSelector,
                        InteriorSelector interiorSelector, BasicCarSelector basicCarSelector,
                        FuelTypeSelector fuelTypeSelector, UserBudgetGetter userBudgetGetter) {
        this.printerWrapper = printerWrapper;
        this.bodySelector = bodySelector;
        this.colorSelector = colorSelector;
        this.interiorSelector = interiorSelector;
        this.basicCarSelector = basicCarSelector;
        this.fuelTypeSelector = fuelTypeSelector;
        this.userBudgetGetter = userBudgetGetter;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Client getClient() {
        return client;
    }

    public CarDatabase getCarDatabase() {
        return carDatabase;
    }

    public void attach(CarSalonMVC.View view) {
        this.view = view;
    }

    public void detach() {
        view = null;
    }

    public void printWelcome() {
        printerWrapper.printWelcome();
    }

    public void start() {
        setClientAndCarDatabase();
        setBasicCar();
        printClientRemainingBudget();
    }

    public void finish() {
        view.showConfiguredCar(car);
    }

    @Override
    public void chooseBody() {
        Body body = bodySelector.choose();
        car.setBody(body);
        throwExceptionLowBudget();
        printClientRemainingBudget();
    }

    @Override
    public void chooseColor() {
        Color color = colorSelector.choose();
        car.setColor(color);
        throwExceptionLowBudget();
        printClientRemainingBudget();
    }

    @Override
    public void chooseInterior() {
        Interior interior = interiorSelector.choose();
        car.setInterior(interior);
        throwExceptionLowBudget();
        printClientRemainingBudget();
    }

    @Override
    public void chooseFuelType() {
        FuelType fuelType = fuelTypeSelector.choose();
        car.setFuelType(fuelType);
        throwExceptionLowBudget();
        printClientRemainingBudget();
    }

    private void setClientAndCarDatabase() {
        this.client = new Client(userBudgetGetter.getBudget());
        this.carDatabase = new CarDatabaseImpl();
        carDatabase.createCarDatabase();
    }

    private void setBasicCar() {
        Map<Integer, Car> mapOfCarsUnderBudget = carDatabase.getCarsUnderBudgetOf(client.getBudget());
        printerWrapper.printMapOfCars(mapOfCarsUnderBudget);
        this.car = basicCarSelector.choose(mapOfCarsUnderBudget);
    }

    private void throwExceptionLowBudget() {
        if (car.calculateCurrentPrice() > client.getBudget()) {
            throw new IllegalStateException("Budget lower than car price!");
        }
    }

    private void printClientRemainingBudget() {
        printerWrapper.printMessage("Client remaining budget: " + (client.getBudget() - car.calculateCurrentPrice()));
    }
}
