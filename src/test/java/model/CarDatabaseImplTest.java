package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarDatabaseImplTest {
    private CarDatabaseImpl tested = new CarDatabaseImpl();

    @BeforeEach
    void setUp() {
        tested.createCarDatabase();
    }

    @Test
    void should_create_map_of_cars() {
        assertFalse(tested.getMapOfCars().isEmpty());
    }

    @Test
    void should_return_quantity_2_for_budget_1500() {
        assertEquals(2,tested.getCarsUnderBudgetOf(1500).size());

    }

}