package controller.componentsselector;

import controller.componentsselector.BodySelector;
import controller.componentsselector.BodySelectorImpl;
import controller.userinput.UserOptionGetter;
import model.components.Body;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BodySelectorImplTest {
    private UserOptionGetter userOptionGetterMock = mock(UserOptionGetter.class);
    private BodySelector tested = new BodySelectorImpl(userOptionGetterMock);

    @Test
    void should_return_SEDAN_for_input_1() {
        when(userOptionGetterMock.getUserInput(Mockito.anyList(), Mockito.anyString())).thenReturn(1);
        Assertions.assertEquals(Body.SEDAN,tested.choose());
    }
}

