package controller.componentsselector;

import controller.userinput.UserOptionGetter;
import model.components.Interior;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class InteriorSelectorImplTest {

    UserOptionGetter userOptionGetterMock = mock(UserOptionGetter.class);
    InteriorSelector tested = new InteriorSelectorImpl(userOptionGetterMock);

    @Test
    void should_return_VELOUR_for_input_1() {
        when(userOptionGetterMock.getUserInput(Mockito.anyList(), Mockito.anyString())).thenReturn(1);
        assertEquals(Interior.VELOUR, tested.choose());
    }
}