package controller.componentsselector;

import controller.userinput.UserOptionGetter;
import model.components.FuelType;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class FuelTypeSelectorImplTest {

    private UserOptionGetter userOptionGetterMock = Mockito.mock(UserOptionGetter.class);
    private FuelTypeSelectorImpl tested = new FuelTypeSelectorImpl(userOptionGetterMock);

    @Test
    void should_return_PETROL_for_input_1 (){
        when(userOptionGetterMock.getUserInput(Mockito.anyList(), Mockito.anyString())).thenReturn(1);
        assertEquals(FuelType.PETROL, tested.choose());

    }
}