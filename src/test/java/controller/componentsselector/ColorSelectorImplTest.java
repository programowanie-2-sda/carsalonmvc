package controller.componentsselector;

import controller.userinput.UserOptionGetter;
import model.components.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ColorSelectorImplTest {
    private UserOptionGetter userOptionGetterMock = mock(UserOptionGetter.class);
    private ColorSelector tested = new ColorSelectorImpl(userOptionGetterMock);

    @Test
    void should_return_RED_for_input_1() {
        when(userOptionGetterMock.getUserInput(Mockito.anyList(), Mockito.anyString())).thenReturn(1);
        Assertions.assertEquals(Color.RED, tested.choose());
    }
}