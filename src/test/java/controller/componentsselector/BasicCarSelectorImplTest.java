package controller.componentsselector;

import controller.userinput.UserOptionGetter;
import model.Car;
import model.CarDatabaseImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class BasicCarSelectorImplTest {
    private UserOptionGetter userOptionGetterMock = Mockito.mock(UserOptionGetter.class);
    private CarDatabaseImpl carDatabase = new CarDatabaseImpl();
    private BasicCarSelectorImpl tested = new BasicCarSelectorImpl(userOptionGetterMock);

    @Test
    void should_return_specific_car_based_on_index_number() {
        when(userOptionGetterMock.getUserInput(Mockito.anyList(),Mockito.anyString())).thenReturn(1);
        Car expected = carDatabase.getMapOfCars().get(1);
        assertEquals(expected,tested.choose(carDatabase.getMapOfCars()));
    }
}