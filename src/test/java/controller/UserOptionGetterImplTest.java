package controller;

import controller.userinput.UserOptionGetter;
import controller.userinput.UserOptionGetterImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.mockito.Mockito.when;

class UserOptionGetterImplTest {
    private ScannerWrapper scannerMock = Mockito.mock(ScannerWrapper.class);
    private PrinterWrapper printerMock = Mockito.mock(PrinterWrapper.class);
    private UserOptionGetter tested = new UserOptionGetterImpl(scannerMock, printerMock);

    @Test
    void should_return_1_for_list_that_contains_1() {
        when(scannerMock.nextInt()).thenReturn(1);
        Assertions.assertEquals(1, tested.getUserInput(List.of(1), Mockito.anyString()));
    }

    @Test
    void should_print_message() {
        when(scannerMock.nextInt()).thenReturn(1);
        tested.getUserInput(List.of(1), Mockito.anyString());
        Mockito.verify(printerMock).printMessage(Mockito.anyString());
    }
}
