package controller;

import controller.userinput.UserBudgetGetterImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class UserBudgetGetterImplTest {

    ScannerWrapper scannerWrapper = Mockito.mock(ScannerWrapper.class);
    PrinterWrapper printerWrapper = Mockito.mock(PrinterWrapper.class);
    UserBudgetGetterImpl tested = new UserBudgetGetterImpl(scannerWrapper,printerWrapper);

    @Test
    void should_return_positive_budget_100() {
        when(scannerWrapper.nextInt()).thenReturn(100);
        assertEquals(100,tested.getBudget());
    }

    @Test
    void should_print_message() {
        when(scannerWrapper.nextInt()).thenReturn(1);
        tested.getBudget();
        Mockito.verify(printerWrapper).printMessage(Mockito.anyString());
    }
}