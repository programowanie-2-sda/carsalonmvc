package controller;

import controller.componentsselector.*;
import controller.userinput.UserBudgetGetter;
import model.Car;
import model.CarDatabase;
import model.CarSalonMVC;
import model.components.Body;
import model.components.Color;
import model.components.FuelType;
import model.components.Interior;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CarSalonImplTest {

    private PrinterWrapper printerWrapperMock = Mockito.mock(PrinterWrapper.class);
    private BodySelector bodySelectorMock = Mockito.mock(BodySelector.class);
    private ColorSelector colorSelectorMock = Mockito.mock(ColorSelector.class);
    private UserBudgetGetter userBudgetGetterMock = Mockito.mock(UserBudgetGetter.class);
    private InteriorSelector interiorSelectorMock = Mockito.mock(InteriorSelector.class);
    private FuelTypeSelector fuelTypeSelectorMock = Mockito.mock(FuelTypeSelector.class);
    private BasicCarSelector basicCarSelectorMock = Mockito.mock(BasicCarSelector.class);
    private CarSalonMVC.View viewMock = Mockito.mock(CarSalonMVC.View.class);
    private CarSalonImpl tested = new CarSalonImpl(printerWrapperMock, bodySelectorMock, colorSelectorMock,
            interiorSelectorMock, basicCarSelectorMock, fuelTypeSelectorMock, userBudgetGetterMock);

    @Test
    void should_set_body_as_SEDAN() {
        when(bodySelectorMock.choose()).thenReturn(Body.SEDAN);
        when(userBudgetGetterMock.getBudget()).thenReturn(1000);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 1000));
        tested.start();
        tested.chooseBody();
        assertEquals(Body.SEDAN, tested.getCar().getBody());
    }

    @Test
    void should_set_color_as_RED() {
        when(colorSelectorMock.choose()).thenReturn(Color.RED);
        when(userBudgetGetterMock.getBudget()).thenReturn(1000);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 1000));
        tested.start();
        tested.chooseColor();
        assertEquals(Color.RED, tested.getCar().getColor());
    }

    @Test
    void should_set_interior_as_VELOUR() {
        when(interiorSelectorMock.choose()).thenReturn(Interior.VELOUR);
        when(userBudgetGetterMock.getBudget()).thenReturn(1000);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 1000));
        tested.start();
        tested.chooseInterior();
        assertEquals(Interior.VELOUR, tested.getCar().getInterior());
    }

    @Test
    void should_set_fuel_type_as_PETROL() {
        when(fuelTypeSelectorMock.choose()).thenReturn(FuelType.PETROL);
        when(userBudgetGetterMock.getBudget()).thenReturn(1000);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        tested.chooseFuelType();
        assertEquals(FuelType.PETROL, tested.getCar().getFuelType());
    }

    @Test
    void should_throw_exception_when_car_price_with_selected_body_higher_than_client_budget_100() {
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        when(bodySelectorMock.choose()).thenReturn(Body.HATCHBACK);
        Assertions.assertThrows(IllegalStateException.class,()->tested.chooseBody());
    }

    @Test
    void should_not_throw_exception_when_car_price_with_standard_body_is_equal_to_client_budget_100() {
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        when(bodySelectorMock.choose()).thenReturn(Body.SEDAN);
        Assertions.assertDoesNotThrow(()->tested.chooseBody());
    }

    @Test
    void should_throw_exception_when_car_price_with_selected_color_higher_than_client_budget_100() {
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        when(colorSelectorMock.choose()).thenReturn(Color.BLACK);
        Assertions.assertThrows(IllegalStateException.class,()->tested.chooseColor());
    }

    @Test
    void should_not_throw_exception_when_car_price_with_standard_color_is_equal_to_client_budget_100() {
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        when(colorSelectorMock.choose()).thenReturn(Color.RED);
        Assertions.assertDoesNotThrow(()->tested.chooseColor());
    }

    @Test
    void should_throw_exception_when_car_price_with_selected_interior_higher_than_client_budget_100() {
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        when(interiorSelectorMock.choose()).thenReturn(Interior.LEATHER_QUILTED);
        Assertions.assertThrows(IllegalStateException.class,()->tested.chooseInterior());
    }

    @Test
    void should_not_throw_exception_when_car_price_with_standard_interior_is_equal_to_client_budget_100() {
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        when(interiorSelectorMock.choose()).thenReturn(Interior.VELOUR);
        Assertions.assertDoesNotThrow(()->tested.chooseInterior());
    }

    @Test
    void should_throw_exception_when_car_price_with_selected_fuel_type_higher_than_client_budget_100() {
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        when(fuelTypeSelectorMock.choose()).thenReturn(FuelType.DIESEL);
        Assertions.assertThrows(IllegalStateException.class,()->tested.chooseFuelType());
    }

    @Test
    void should_not_throw_exception_when_car_price_with_standard_fuel_type_is_equal_to_client_budget_100() {
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        when(fuelTypeSelectorMock.choose()).thenReturn(FuelType.PETROL);
        Assertions.assertDoesNotThrow(()->tested.chooseFuelType());
    }

    @Test
    void should_print_configured_car() {
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.attach(viewMock);
        tested.start();
        tested.finish();
        Mockito.verify(viewMock).showConfiguredCar(tested.getCar());
    }

    @Test
    void should_set_budget_for_client_as_100() {
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        Assertions.assertEquals(100, tested.getClient().getBudget());
    }

    @Test
    void should_show_client_remaining_budget_as_0_when_initial_budget_was_100_and_basic_car_price_is_100(){
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        Mockito.verify(printerWrapperMock).printMessage("Client remaining budget: 0");
    }

    @Test
    void should_set_car_database(){
        when(userBudgetGetterMock.getBudget()).thenReturn(100);
        when(basicCarSelectorMock.choose(Mockito.anyMap())).thenReturn(new Car("car", 100));
        tested.start();
        Assertions.assertFalse(tested.getCarDatabase().getMapOfCars().isEmpty());
    }

}